package com.example.tp1_3_java;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.Manifest.permission.CALL_PHONE;
import static android.graphics.Color.argb;

public class Activity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        final Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");

        TextView t=(TextView)findViewById(R.id.textView);
        String phone=bundle.get("phone").toString();

        //Toast.makeText(Activity3.this,phone,Toast.LENGTH_LONG).show();
        t.setText(phone);


       // EditText phone=(EditText)findViewById(R.id. phone);
        Button button=(Button)findViewById(R.id.button3);
        Uri uri=Uri.parse("tel:"+phone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Activity3.this,"onclick ",Toast.LENGTH_LONG).show();
                Intent callIntent = new Intent(Intent.ACTION_CALL,uri);
                if (ContextCompat.checkSelfPermission(Activity3.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),"Pas les droits pour appeler ",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Appel en cours ",Toast.LENGTH_LONG).show();
                    startActivity(callIntent);
                }
                }

        });
    }
}