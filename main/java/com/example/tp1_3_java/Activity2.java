package com.example.tp1_3_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.graphics.Color.argb;

public class Activity2 extends AppCompatActivity {

    private Button button,button2;
    private TextView firstNameTV, lastNameTV,ageTV,competenceTV,phoneTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        final Intent intent2 = new Intent(this, Activity3.class);
        final Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");

        String firstName=bundle.get("firstName").toString();
        String lastName=bundle.get("lastName").toString();
        String age=bundle.get("age").toString();
        String competence=bundle.get("competence").toString();
        String phone=bundle.get("phone").toString();



        firstNameTV=(TextView)findViewById(R.id.firstName);
        lastNameTV=(TextView)findViewById(R.id.lastName);
        ageTV=(TextView)findViewById(R.id.age);
        competenceTV=(TextView)findViewById(R.id.competence);
        phoneTV=(TextView)findViewById(R.id. phone);
        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        firstNameTV.setText(firstName);
        lastNameTV.setText(lastName);
        ageTV.setText(age);
        competenceTV.setText(competence);
        phoneTV.setText(phone);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle B=new Bundle();
                B.putString("phone",phone);
                intent2.putExtra("data", B);
                startActivity(intent2);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}