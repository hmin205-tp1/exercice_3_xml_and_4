package com.example.tp1_3_java;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import static android.graphics.Color.argb;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private EditText firstName, lastName,age,competence,phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent intent = new Intent(this, Activity2.class);

        firstName=(EditText)findViewById(R.id.firstName);
        lastName=(EditText)findViewById(R.id.lastName);
        age=(EditText)findViewById(R.id.age);
        competence=(EditText)findViewById(R.id.competence);
        phone=(EditText)findViewById(R.id. phone);
        button = (Button) findViewById(R.id.button);

        AlertDialog.Builder builder= new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.validation);
        builder.setTitle(R.string.titleDialog);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,int which)
                            {
                                boolean completed=true;
                                if(firstName.getText().toString().equals("")) {
                                    completed=false;
                                }
                                if(lastName.getText().toString().equals("")) {
                                    completed=false;
                                }
                                if(age.getText().toString().equals("")) {
                                    completed=false;
                                }
                                if(competence.getText().toString().equals("")) {
                                    completed=false;
                                }
                                if(phone.getText().toString().equals("")) {
                                    completed=false;
                                }
                                if(completed)
                                {
                                    Bundle B=new Bundle();
                                    B.putString("firstName",firstName.getText().toString());
                                    B.putString("lastName",lastName.getText().toString());
                                    B.putString("age",age.getText().toString());
                                    B.putString("competence",competence.getText().toString());
                                    B.putString("phone",phone.getText().toString());
                                    intent.putExtra("data", B);

                                    startActivity(intent);
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, R.string.notComplete, Toast.LENGTH_LONG).show();
                                    dialog.cancel();
                                }
                            }
                        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which)
            {
                if(!firstName.getText().toString().equals(""))
                    firstName.setBackgroundColor(argb(0, 0, 0, 0));
                if(!lastName.getText().toString().equals(""))
                    lastName.setBackgroundColor(argb(0, 0, 0, 0));
                if(!age.getText().toString().equals(""))
                    age.setBackgroundColor(argb(0, 0, 0, 0));
                if(!competence.getText().toString().equals(""))
                    competence.setBackgroundColor(argb(0, 0, 0, 0));
                if(!phone.getText().toString().equals(""))
                    phone.setBackgroundColor(argb(0, 0, 0, 0));

                dialog.cancel();
            }});
        AlertDialog alertDialog = builder.create();



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firstName.getText().toString().equals(""))
                    firstName.setBackgroundColor(argb(60, 150, 30, 30));
                else
                    firstName.setBackgroundColor(argb(60, 30, 30, 30));
                if(lastName.getText().toString().equals(""))
                    lastName.setBackgroundColor(argb(60, 150, 30, 30));
                else
                    lastName.setBackgroundColor(argb(60, 30, 30, 30));
                if(age.getText().toString().equals(""))
                    age.setBackgroundColor(argb(60, 150, 30, 30));
                else
                   age.setBackgroundColor(argb(60, 30, 30, 30));
                if(competence.getText().toString().equals(""))
                    competence.setBackgroundColor(argb(60, 150, 30, 30));
                else
                    competence.setBackgroundColor(argb(60, 30, 30, 30));
                if(phone.getText().toString().equals(""))
                    phone.setBackgroundColor(argb(60, 150, 30, 30));
                else
                    phone.setBackgroundColor(argb(60, 30, 30, 30));
                alertDialog.show();
                Toast.makeText(MainActivity.this, R.string.toasted, Toast.LENGTH_LONG).show();
            }
        });
    }
}